# GoogleCloudRecycleAPI

This is a python API that you can run locally or wirelessly that tells you is an image is recycling trash or compost

## Dependencies

You need to indtall the dependencies using `pip install (module)`
- sys
- os
- base64
- json
- flask_restful
- flask
- threading
- urllib

## Quick Start

Make a POST request with a base64 image in the body to https://APICLOUDML--hacker22.repl.co/api for a output of `["typeOfWaste","IntPrecisionScore"]`

## How it works

It uses my cloud hosted Machine Learning Model and connects it using the credentials so anyone can use it! You just need to POST the /api and in the HTTPS request body, have the base64 image you want and it will return `["result","precision_score"]` and the results can be

- recyling_paper
- recyling_plastic
- recyling_metal
- recyling_cardboard
- recyling_glass
- trash
- compost

 
## Data
The data repo is right [here](https://gitlab.com/Hacker719/RecycleML-Data) and still make sure to give credit for use!

## Credit

Feel free to use how you see fit and make sure to ALWAYS give credit and access to original repo

