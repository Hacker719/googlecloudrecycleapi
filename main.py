import sys
import os
import base64 
import json
from flask_restful import Resource, Api
from flask import Flask
from flask import request
from threading import Thread
import urllib 
app = Flask('')
api = Api(app)
os.environ["GOOGLE_APPLICATION_CREDENTIALS"]='/home/runner/cred.json'
from google.cloud import automl_v1beta1
from google.cloud.automl_v1beta1.proto import service_pb2

def get_prediction(basedata):
	prediction_client = automl_v1beta1.PredictionServiceClient()
	name = 'projects/{}/locations/us-central1/models/{}'.format('recyclesmart', 'ICN1555079590238864874')
	with open("image.jpg", "wb") as fh:
		fh.write(base64.b64decode(basedata))
	with open('/home/runner/image.jpg', "rb") as image_file:
		content = image_file.read()
	payload = {'image': {'image_bytes': content}}
	params = {}
	request = prediction_client.predict(name, payload, params)
	return request  # waits till request is returned

@app.route('/api', methods=['POST'])
def get():
	things = []
	response = get_prediction(request.data)
	for result in response.payload:
		things.append(result.display_name)
		things.append(int(100*result.classification.score))
	print(things)
	return json.dumps(things)
	

def run():
  app.run(host='0.0.0.0',port=3000)

  
t = Thread(target=run)
t.start()